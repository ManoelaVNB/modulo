### Pesquisa de Clima Operacional

- Este módulo imita uma pesquisa de clima operacional
-  Consiste de um formulário onde o usuário (funcionário) deve digitar seu nome, selecionar seu departamento e selecionar uma resposta para cada pergunta (5 no total), ele também pode adicionar qualquer queixa que tenha em um campo de texto.
- Em teoria o funcionário não tem acesso às informações do banco de dados, então as funções de deletar e ver todas as respostas não foram adicionadas.

### Banco de dados

- Este módulo exige um banco de dados para armazenar as pesquisas realizadas
- a tabela 'clima_pesquisa' deve ser acrescentada dentro de 'lp2_modulo'
- a tabela em quetão se encontra na pasta 'sql'

### URLs

- Para acesssar a página de pesquisa, digite a url: 'http://localhost/modulo/clima/Clima'.
- Para acesssar a página de testes, digite a url: 'http://localhost/modulo/clima/test/all'.

