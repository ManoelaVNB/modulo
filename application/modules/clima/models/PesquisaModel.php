<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class PesquisaModel extends CI_Model{
    /**
     * Registra uma pesquisa no banco
     * @return boolean true caso ocorra erro de validação
     */
    public function nova_pesquisa(){
        if(! sizeof($_POST));

        $this->load->library('PesquisaValidator', null, 'valida');

        if($this->valida->form_pesquisa()){
            $this->load->library('PesquisaClima', null, 'pesquisa');

            $data = $this->input->post();

            $this->pesquisa->insert($data);
        }
        else return true;
    }
}