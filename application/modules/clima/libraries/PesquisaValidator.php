<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class PesquisaValidator extends CI_Object{

    public function form_pesquisa(){
        $this->form_validation->set_rules('nome', 'nome', 'required|max_length[100]|min_length[2]');
        $this->form_validation->set_rules('departamento', 'departamento', 'required');
        $this->form_validation->set_rules('grupo', 'grupo', 'required');
        $this->form_validation->set_rules('mudanca', 'mudanca', 'required');
        $this->form_validation->set_rules('informacoes', 'informacoes', 'required');
        $this->form_validation->set_rules('reunioes', 'reunioes', 'required');
        $this->form_validation->set_rules('lideranca', 'lideranca', 'required');
        $this->form_validation->set_rules('adicional', 'adicional', 'max_length[300]');
        return $this->form_validation->run();
    }

}