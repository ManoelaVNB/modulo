<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class PesquisaClima extends Dao {

    function __construct(){
        parent::__construct('clima_pesquisa');
    }

    public function insert($data, $table = null) {
        $cols = array('nome', 'departamento', 'grupo', 'mudanca', 'informacoes', 'reunioes', 'lideranca', 'adicional');
        $this->expected_cols($cols);

        return parent::insert($data);
    }
}