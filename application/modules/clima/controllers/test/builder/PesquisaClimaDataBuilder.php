<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class PesquisaClimaDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo_test'){
        parent::__construct('clima_pesquisa', $table);
    }

    function getData($index = -1){
        $data[0]['nome'] = 'Maria';
        $data[0]['departamento'] = 'TI';
        $data[0]['grupo'] = 'sin';
        $data[0]['mudanca'] = 'nao';
        $data[0]['informacoes'] = 'as vezes';
        $data[0]['reunioes'] = 'sim';
        $data[0]['lideranca'] = 'nao';
        $data[0]['adicional'] = 'sem queixas';

        $data[1]['nome'] = 'Maria';
        $data[1]['departamento'] = null;
        $data[1]['grupo'] = 'sin';
        $data[1]['mudanca'] = 'nao';
        $data[1]['informacoes'] = null;
        $data[1]['reunioes'] = 'sim';
        $data[1]['lideranca'] = 'nao';
        $data[1]['adicional'] = 'sem queixas';

        $data[2]['nome'] = 'Maria';
        $data[2]['departamento'] = 'TI';
        $data[2]['grupo'] = 'sin';
        $data[2]['mudanca'] = 'nao';
        $data[2]['informacoes'] = 'as vezes';
        $data[2]['reunioes'] = 'sim';
        $data[2]['lideranca'] = 'nao';
        $data[2]['adicional'] = null;

        return $index > -1 ? $data[$index] : $data;
    }
}