<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/clima/controllers/test/builder/PesquisaClimaDataBuilder.php';
include_once APPPATH . 'modules/clima/libraries/PesquisaClima.php';

class PesquisaClimaTest extends Toast{
    //o builder só é usado para passar os dados e limpar a tabela
    private $builder;
    private $pesquisa;

    function __construct(){
        parent::__construct('ClimaTest');
    }

    function _pre(){
        $this->builder = new PesquisaClimaDataBuilder();
        $this->pesquisa = new PesquisaClima();
    }

    //TESTES DE INTEGRAÇÃO
    //ps: o funcionario não tem acesso aos registros na tabela

    // apenas ilustrativo
    function test_classe_PesquisaClima_criada(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->pesquisa, "Erro na criação da pesquisa");
    }

    function test_insere_na_tabela(){
        //cenário 1: dados corretos
        $this->builder->clean_table();
        $dados1 = $this->builder->getData(0);
        $id1 = $this->pesquisa->insert($dados1);
        $this->_assert_equals(1, $id1, "Esperado id, recebido $id1");

        //cenário 2: mandar vazio
        //se $dados vazio deve receber -1
        $dados = array();
        $id2 = $this->pesquisa->insert($dados);
        $this->_assert_equals(-1, $id2, "Esperado id, recebido $id2");

        /**
         * cenário 3: mandar nulo onde exige quenão seja
         * a maioria dos dados são enviados por botão, com opcoes limitadas em cada um
         * se o usuario nao os seleciona a informação não vai, portanto o teste foi feito
         * utilizando o parametro 'null', que bate com a segunda checagem (numero de colunas), 
         * que devolve -2.
         */
        $dados = $this->builder->getData(1);
        $id2 = $this->pesquisa->insert($dados);
        $this->_assert_equals(-2, $id2, "Esperado id, recebido $id2");

        /**
         * cenário 4: o unico campo que pode não ser digitado é 'adicional'
         * ele vai como vazio, mas não aceita nulo (erro no numero de colunas)
         */
        $dados = $this->builder->getData(2);
        $id2 = $this->pesquisa->insert($dados);
        $this->_assert_equals(-2, $id2, "Esperado id, recebido $id2");
        $dados['adicional'] = ''; //correcao do erro
        $this->_assert_equals_strict('', $dados['adicional'], "Esperado '', recebido: ".$dados['adicional']);

        /**
         * cenário 5: checar se o registro inserido é o desejado
         * para o teste foram usados o id e dados
         * do cenario 1, que realmente insere
         */
        $exemp = $this->pesquisa->get(array('id' => 1))[0];
        $this->_assert_equals($dados1['nome'], $exemp['nome'], "Esperado:".$dados1['nome']." Recebido: ".$exemp['nome']);
        $this->_assert_equals($dados1['departamento'], $exemp['departamento'], "Esperado:".$dados1['departamento']." Recebido: ".$exemp['departamento']);

    }
}