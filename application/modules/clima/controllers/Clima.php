<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Clima extends MY_Controller{

    /**
     * O controller manda os dados para criar os botoes do formulario
     */
    public function index(){
        $this->load->view('common/cabecalho', true);

        $this->load->model('PesquisaModel', 'model');
        $this->model->nova_pesquisa();

        $data['texto'] = 'Meu grupo de trabalho se relaciona com harmonia.';
        $data['nome'] = 'grupo';
        $v['radio1'] = $this->load->view('radio_buttons', $data, true);

        $data2['texto'] = 'Sou informado sobre as mudanças de processos que afetam o meu trabalho.';
        $data2['nome'] = 'mudanca';
        $v['radio2'] = $this->load->view('radio_buttons', $data2, true);

        $data3['texto'] = 'Tenho acesso a todas as informações e ferramentas que preciso para realizar minhas funções.';
        $data3['nome'] = 'informacoes';
        $v['radio3'] = $this->load->view('radio_buttons', $data3, true);

        $data4['texto'] = 'Há reuniões periódicas entre os gestores e os colaboradores do meu time.';
        $data4['nome'] = 'reunioes';
        $v['radio4'] = $this->load->view('radio_buttons', $data4, true);

        $data5['texto'] = 'Confio na postura de liderança do meu gestor.';
        $data5['nome'] = 'lideranca';
        $v['radio5'] = $this->load->view('radio_buttons', $data5, true);

        $this->load->view('form_pesquisa',$v);

        $this->load->view('common/rodape', true);
    }

    public function proximo(){
        $this->load->view('common/cabecalho', true);
        
        $this->load->view('reiniciar_pesquisa');

        $this->load->view('common/rodape', true);
    }
}