<div class="container mt-5 mb-5">
    <div class="jumbotron">
        <h2 class="display-4">Obrigada por responder a Pesquisa!</h2>
        <p class="lead">Sua resposta foi armazenada!</p>
        <hr class="my-4">
        <p>Para voltar para a pesquisa clique no botão 'Pesquisa'.</p>
        <div class="pt-2">
            <a href="<?= base_url('clima/Clima')?>"><button type="button" class="btn btn-outline-grey waves-effect">Pesquisa</button></a>
            <a href="<?= base_url('clima/test/all')?>"><button type="button" class="btn btn-outline-red waves-effect">Testes</button></a>
        </div>
    </div>
</div>