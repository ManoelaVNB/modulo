<p class="h5 bold"><?= $texto ?></p>
<div class="custom-control custom-radio">
    <input type="radio" class="custom-control-input" id="<?= $nome ?>_sim" name="<?= $nome ?>" value="sim">
    <label class="custom-control-label" for="<?= $nome ?>_sim">Sim</label>
</div>
<div class="custom-control custom-radio">
    <input type="radio" class="custom-control-input" id="<?= $nome ?>_nao" name="<?= $nome ?>" value="nao">
    <label class="custom-control-label" for="<?= $nome ?>_nao">Não</label>
</div>
<div class="custom-control custom-radio">
    <input type="radio" class="custom-control-input" id="<?= $nome ?>_asvezes" name="<?= $nome ?>" value="as vezes">
    <label class="custom-control-label" for="<?= $nome ?>_asvezes">Às Vezes</label>
</div>