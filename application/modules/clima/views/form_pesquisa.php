<div class="container mt-5 mb-5">
    <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    <div class="card">
        <div class="card-header">
            <h3 class="text-center">Pesquisa de Clima Operacional</h3>
        </div>
        <div class="card-body">
         
            <form method="POST" class="text-left border border-light p-4" id="task-form">
                <div class="form-row">
                    <div class="col-md-6">
                        <input type="text" class="form-control" value="" id="nome" name="nome" placeholder="Nome">
                    </div>

                    <div class="col-md-6">
                        <select name="departamento" class="browser-default custom-select mb-4">
                            <option  value="" selected disabled>Departamento</option>
                            <option name="departamento" value="vendas">Vendas</option>
                            <option name="departamento" value="RH">Recursos Humanos</option>
                            <option name="departamento" value="TI">TI</option>
                            <option name="departamento" value="Financeiro">Financeiro</option>
                            <option name="departamento" value="Adm">Administrativo</option>
                            <option name="departamento" value="outro">Outro</option>
                        </select>
                    </div>
                </div>
                <br>

                <div class="form-row">
                    <div class="col">
                        <?= $radio1 ?>
                    </div>
                </div>
                <br>

                <div class="form-row">
                    <div class="col">
                        <?= $radio2 ?>
                    </div>
                </div>
                <br>

                <div class="form-row">
                    <div class="col">
                        <?= $radio3 ?>
                    </div>
                </div>
                <br>

                <div class="form-row">
                    <div class="col">
                        <?= $radio4 ?>
                    </div>
                </div>
                <br>

                <div class="form-row">
                    <div class="col">
                        <?= $radio5 ?>
                    </div>
                </div>
                <br>

                <div class="form-row mb-4">
                    <div class="col-md-12">
                        <textarea name="adicional" class="form-control" id="adicional" rows="4" placeholder="Queixas adicionais"></textarea>
                    </div>
                </div>
                <div class="text-center text-md-left">
                    <a class="btnupload-form btn btn-grey" onclick="document.getElementById('task-form').submit();">Enviar</a>
                    <a href="proximo" class="btnupload-form btn btn-grey">Próximo</a>
                </div>
            </form>
        </div>
    </div>
</div>